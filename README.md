# PGN to Mind Map

Takes a PGN file and converts it into a mind map like this:

```plantuml
@startmindmap
*[#B1D9F9] Game Start
**[#DDDDDD] 1.e4
***[#AAAAAA] 1...c6
****[#DDDDDD] 2.Nc3
*****[#AAAAAA] 2...d5
******[#DDDDDD] 3.Nf3
*******[#AAAAAA] 3...dxe4
********[#DDDDDD] 4.Nxe4
*********[#AAAAAA] 4...Nf6
****[#DDDDDD] 2.d4
*****[#AAAAAA] 2...d5
******[#DDDDDD] 3.Nc3
*******[#AAAAAA] 3...dxe4
********[#DDDDDD] 4.Nxe4
*********[#AAAAAA] 4...Nf6
******[#DDDDDD] 3.exd5
*******[#AAAAAA] 3...cxd5
********[#DDDDDD] 4.c4
*********[#AAAAAA] 4...Nf6
********[#DDDDDD] 4.Bd3
*********[#AAAAAA] 4...Nc6
******[#DDDDDD] 3.f3
*******[#AAAAAA] 3...Qb6
********[#DDDDDD] 4.Nc3
*********[#AAAAAA] 4...dxe4
******[#DDDDDD] 3.e5
*******[#AAAAAA] 3...c5
********[#DDDDDD] 4.c3
*********[#AAAAAA] 4...Nc6
********[#DDDDDD] 4.dxc5
*********[#AAAAAA] 4...e6
@endmindmap
```

It outputs [PlantUML](https://www.plantuml.com/).

## Initial Setup

You will need the python `python-chess` module installed

```bash
pip install python-chess
```

## Running

For now, the name of the PGN file is hard-coded and must be `input.pgn` (idea for future enhancement)

```bash
python diagram.py
```

This will display the PlantUML you can use.

## Tips

### Merging PGNs

If you have multiple PGNs, you can first combine them with a tool like [Merge-PGN](https://github.com/permutationlock/merge-pgn).

### Displaying PlantUML

You can use PlantUML's [online demo server](https://www.plantuml.com/plantuml/uml/SyfFKj2rKt3CoKnELR1Io4ZDoSa70000) to see the output.

GitLab also can render PlantUML directly, which is how this README works