"""
Takes input.pgn file and output plantUML Mind Map
"""

import math
import chess.pgn

WHITE_STYLE_COLOR = "#DDDDDD"
BLACK_STYLE_COLOR = "#AAAAAA"

def main():
    """
    Start the program by reading the input.pgn file and kicking off the recursive call
    """
    with open("input.pgn", encoding="utf-8") as pgn_file:
        game = chess.pgn.read_game(pgn_file)

    print("@startmindmap")
    print_variations(game)
    print("@endmindmap")

def print_variations(node):
    """
    Recursively prints the nodes and variations
    """
    if node.move is None:
        print("*[#B1D9F9] Game Start")
    else:
        board = node.board()
        if node.turn() == chess.WHITE:
            color = BLACK_STYLE_COLOR #white's turn to move means black just moved
            move_number = f"{math.floor(board.ply() / 2)}..."
        else:
            color = WHITE_STYLE_COLOR
            move_number = f"{math.ceil(board.ply() / 2)}."

        print(f"{'*'*(board.ply() + 1)}[{color}] {move_number}{node.san()}")

    for next_variation in node.variations:
        #recursive
        print_variations(next_variation)

main()
